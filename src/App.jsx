import React from 'react';
import { IndexRouter } from '@components/routers/Index.router';
import 'react-toastify/dist/ReactToastify.css';
export const App = () => {
  return <IndexRouter />;
};
