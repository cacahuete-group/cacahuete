export default function authHeader() {
  const key = JSON.parse(localStorage.getItem('user'));

  if (key.user && key.user.token) {
    return { Authorization: key.user.token };
  } else {
    return {};
  }
}
