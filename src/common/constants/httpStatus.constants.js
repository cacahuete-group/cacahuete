export const HTTP_STATUS = {
  PROGRESS: 'progress',
  SUCCESS: 'success',
  FAIL: 'error',
};
