export const AUTHENTICATION = {
  IS_CONNECTED: 'isConnected',
  IS_ADMIN: 'isAdmin',
  ADMIN_TYPE: 'admin',
  USER_TYPE: 'user',
};
