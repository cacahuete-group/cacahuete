export const API_URL =
  process.env.REACT_APP_API ?? 'http://localhost:8010/proxy';

export const API_ROUTES = {
  GET_JOURNEYS: '/journeys',

  GET_USER_JOURNEYS: '/journey',
};
