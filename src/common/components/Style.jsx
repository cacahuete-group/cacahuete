import { makeStyles } from '@mui/styles';

export const UseStyles = makeStyles({
  searchbar: {
    border: 0,
    boxShadow: '0px 0px 15px 5px rgba(0, 0, 0, 0.25)',
    height: 60,
    padding: '0 30px',
    color: '#370617',
    fontFamily: 'Comfortaa',
    fontSize: '1.5rem',
    width: '40rem',
  },
});
