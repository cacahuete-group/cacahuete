// import { Typography } from "@containers/Typography";
// import {Title} from '@containers/Title'
// import React from "react"
// import { SecondaryButton } from "./SecondaryButton"
// import Modal from '@mui/material/Modal';
// import { Button } from "@containers/Button";
// import { faTimes } from "@fortawesome/free-solid-svg-icons";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

// export const DangerZone = (
//   action,
//   show
// ) => {

//   const [open, setOpen] = React.useState(show);
//   const handleOpen = () => setOpen(true);
//   const handleClose = () => setOpen(false);

//   const handleClick = () => {
//     action();
//   }

//   return(
// <>
//      <div>
//       <Modal
//         open={open}
//         onClose={handleClose}
//         aria-labelledby="modal-modal-title"
//         aria-describedby="modal-modal-description"
//       >

//         <div className="px-5 pt-5 top-96 left-1/4 absolute w-1/2 bg-white rounded-lg shadow-lg ">
//         <div className="w-full flex flex-row justify-end">

//         <FontAwesomeIcon icon={faTimes} className={"text-gray-icons right-5 cursor-pointer"} size="1x" onClick={handleClose} />
//         </div>
//         <div className="flex flex-col justify-center items-center">
//           <Title
//           font="ubuntu"
//           color="primary-default"
//           size="h2"
//           bold
//           >
//             Delete my account

//           </Title>
//           <Typography
//             color="gray-primary"
//             font="comfortaa"
//             className="mt-10 mb-3"
//           >
//             Are you sure ?
//           </Typography>
//           <Typography
//             color="text-secondary-default"
//             font="comfortaa"
//             className="mb-5"
//             bold
//           >
//           ⚠️ This action can't be undone
//           </Typography>
//           <div className="w-full flex flex-row justify-center space-x-8">
//           <SecondaryButton
//             action={action}
//           >
//             Delete
//           </SecondaryButton>
//           <Button
//             color="bg-gray-icons"
//             hover_color="bg-gray-primary"
//             action={handleClose}
//           >
//             Cancel
//           </Button>
//           </div>
//         </div>
//         </div>
//       </Modal>
//     </div>
//     </>
//   )
// }
