export const isPostalCodeValid = (postal_code) => /^\d{5}$/.test(postal_code);

export const isBeginPostalCode = (postal_code) => /^\d{2,5}$/.test(postal_code);
