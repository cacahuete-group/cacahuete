import { useContext, useEffect, useReducer, useRef, useState } from 'react';
import { buildRoute } from '@utils/routes.util';
import axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';
import { SET_HTTP, DELETE_HTTP, ADD_JOURNEYS } from '@store/types';
import { HTTP_STATUS } from '@constants/httpStatus.constants';
import { API_ROUTES } from '@constants/routes';
export const useJourneys = (perPage, page) => {
  const [loading, setLoading] = useState(true);
  const [fetchFinish, setFetchFinish] = useState(false);
  //const [fetchStart, setFetchStart] = useState(true);

  const dispatch = useDispatch();

  const route = buildRoute(
    API_ROUTES.GET_JOURNEYS,
    `per_page=${perPage}`,
    `page=${page}`,
  );

  //const hasFetchedJourneys = useRef(false);

  useEffect(() => {
    //setFetchFinish(false);
    setLoading(true);
    //hasFetchedJourneys.current = false;
    //dispatch({SET_HTTP, payload: { status: HTTP_STATUS.PROGRESS} })
    axios
      .get(route)
      .then(async (res) => {
        // if (!res.response) {
        //   dispatch({ SET_HTTP, payload: { status: HTTP_STATUS.ERROR, statusCode: res.status, message: res.data } })
        //     type: "failed",
        //     error: {
        //       message: res.status,
        //       code: res.status,
        //     },
        //   });
        //   return;
        // }
        //hasFetchedJourneys.current = true;

        dispatch({
          type: ADD_JOURNEYS,
          payload: {
            journeys: res.data.journeys,
            count: res.data.count,
            loadedJourney: res.data.journeys.length,
          },
        });
        //dispatch({SET_HTTP, payload: {status: HTTP_STATUS.SUCCESS, statusCode: 200} })
      })
      .catch((err) => {
        //dispatch({SET_HTTP, payload: {status: HTTP_STATUS.FAIL, statusCode: err.statusCode} })
      })
      .finally(setLoading(false));
  }, [route]);

  return {
    allLoaded: useSelector((state) => state.journeys.allLoaded),
    loading,
    loaded: useSelector((state) => state.journeys.loaded),
    journeys: useSelector((state) => state.journeys.allJourneys),
    count: useSelector((state) => state.journeys.count),
  };
};
