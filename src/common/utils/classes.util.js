export const cn = (...classes) =>
  classes.filter((c) => typeof c !== 'boolean').join(' ');
