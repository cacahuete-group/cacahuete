module.exports = {
    content: ["./src/**/*.{html,js,jsx}"],
    //purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
    theme: {
        fontFamily: {
            ubuntu: ["Ubuntu"],
            comfortaa: ["Comfortaa"],
        },
        extend: {
            fontSize: {
                "4xl": "4rem",
                "3xl": "2.625rem",
                "2xl": "2.25rem",
                "lg": "1.5rem",
            },
            colors: {
                primary: {
                    default: "#370617",
                },
                secondary: {
                    default: "#900606",
                },
                gray: {
                    primary: "708C91", //Color for paragraphs
                    secondary: "#BBBBBB", //Color for hr
                    icons: "#66676B", //Color for icons
                    disabled: "#66676B",
                },
                blue: {
                    primary: "#0096C7",
                    secondary: "#0077B6",
                },


                // 'primary-red': '#370617',
                // 'secondary-red': '#900606',
                // 'primary-gray': '708C91', //COlor for paragraphs
                // 'secondary-gray': '#BBBBBB',  //Color for hr
                // 'third-gray': '#66676B' //Color for icons on cards
            },
            boxShadow: {
                "lg": "0px 0px 15px 5px rgba(0, 0, 0, 0.25)",
            },
        },

    },
    plugins: [],
};
