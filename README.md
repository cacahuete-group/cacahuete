# Cacahuete Frontend

Frontend part of the project Cacahuète. Made with React and a bit of imagination ☁️.  
To get more infos on this project, check the documentation : [here](https://gitlab.com/cacahuete-group/cacahuete-documentation)

*Created in 2022 by GridexX with ❤*

More to come...
